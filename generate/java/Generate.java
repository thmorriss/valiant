
// ideas:
// after a certain point, have guard ants
// have different classes of ant
// if you ever reach the enemy base, just sit on it
// advanced: go in pairs of two (leading/following, special pheremones for home base spot)
class Generate {
    // 6 bits pheromone, 8 bits memory
    //memory bits
    // 1 initialized, 2 direction, 1 guard status, 1 random bit
    // pheremone bits general
    // 2 direction,
    // pheremone bits home base
    // 2 direction? 3 counting bits for how many
    public static void main(String[] args) {
        makeAi((memory, pheromone, inFront, hasFood, isEast, isStacked, random) -> {
                Boolean last_ranom = memory[4];
                memory[4] = random;

                if(memory[7]) {
                    return new Output(memory, pheromone, Action.STAY_STILL);
                }

                // If not initialized, right until we face east
                if(!memory[0]) {
                    if(!isEast) {
                        return new Output(memory, pheromone, Action.TURN_RIGHT);
                    }
                    //homebase pheremone
                    int count = decodeInt(pheromone, 3, 3);
                    if(count < 8) {
                        encodeInt(pheromone, 3, 3, count + 1);
                        // become normal ant
                        memory[7] = false;
                    } else {
                        // become guard ant with 1/4 chance

                        if (random && last_ranom)
                            memory[7] = true;
                    }

                    memory[0] = true;
                    encodeInt(memory, 1, 2, 1);
                }

                int direction = decodeInt(memory, 1, 2);

                // If pheromone not set, set it to the opposite of our direction
                if(!pheromone[0]) {
                    pheromone[0] = true;
                    encodeInt(pheromone, 1, 2, direction + 2);
                }

                // If we have food, go home, otherwise, wander randomly
                Action action;
                if(hasFood) {
                    // If we are facing the direction the pheromone tells us, go straight
                    if(decodeInt(memory, 1, 2) == decodeInt(pheromone, 1, 2)) {
                        action = Action.MOVE_FORWARD;
                        // Otherwise, turn right
                    } else {
                        encodeInt(memory, 1, 2, direction + 1);
                        action = Action.TURN_RIGHT;
                    }
                    // Randomly go forward
                } else if(random) {
                    action = Action.MOVE_FORWARD;
                    // Or right
                } else {
                    encodeInt(memory, 1, 2, direction + 1);
                    action = Action.TURN_RIGHT;
                }
                return new Output(memory, pheromone, action);
            });
    }

    static void encodeInt(boolean[] array, int offset, int length, int value) {
        for(int i = 0; i < length; i++) {
            array[offset + i] = (value & (1 << i)) != 0;
        }
    }

    static int decodeInt(boolean[] array, int offset, int length) {
        int result = 0;
        for(int i = 0; i < length; i++) {
            if(array[offset + i]) {
                result |= 1 << i;
            }
        }
        return result;
    }




    static final int MEMORY_BITS = 8;
    static final int PHEROMONE_BITS = 6;

    interface AI {
        Output decide(boolean[] memory, boolean[] pheromone, InFront inFront, boolean hasFood, boolean isEast, boolean isStacked, boolean random);
    }

    enum InFront { EMPTY, WALL, FOOD, FRIENDLY_ANTHILL, ENEMY_ANTHILL, FRIEND, ENEMY };

    static class Output {
        boolean[] memory;
        boolean[] pheromone;
        Action action;

        public Output(boolean[] memory, boolean[] pheromone, Action action) {
            if(memory.length != MEMORY_BITS) throw new IllegalArgumentException("Illegal memory length");
            if(pheromone.length != PHEROMONE_BITS) throw new IllegalArgumentException("Illegal pheromone length");
            this.action = action;
            this.memory = memory;
            this.pheromone = pheromone;
        }
    }

    enum Action { STAY_STILL, TURN_LEFT, TURN_RIGHT, MOVE_FORWARD }

    static void makeAi(AI ai) {
        int[] rules = new int[1 << 21];

        for(int memoryBits = 0; memoryBits < 1 << MEMORY_BITS; memoryBits++) {
            for(int pheromoneBits = 0; pheromoneBits < 1 << PHEROMONE_BITS; pheromoneBits++) {
                for(boolean hasFood : new boolean[] {false, true}) {
                    for(boolean isEast : new boolean[] {false, true}) {
                        for(boolean isStacked : new boolean[] {false, true}) {
                            for(boolean random : new boolean[] {false, true}) {
                                for(InFront inFront : InFront.values()) {
                                    boolean[] memory = new boolean[MEMORY_BITS];
                                    for(int i = 0; i < MEMORY_BITS; i++) {
                                        memory[i] = (memoryBits & (1 << i)) != 0;
                                    }
                                    boolean[] pheromone = new boolean[PHEROMONE_BITS];
                                    for(int i = 0; i < PHEROMONE_BITS; i++) {
                                        pheromone[i] = (pheromoneBits & (1 << i)) != 0;
                                    }

                                    Output output = ai.decide(memory, pheromone, inFront, hasFood, isEast, isStacked, random);

                                    int index = 0;
                                    if(random) {
                                        index |= 1;
                                    }
                                    index <<= 1;
                                    if(isStacked) {
                                        index |= 1;
                                    }
                                    index <<= 1;
                                    if(isEast) {
                                        index |= 1;
                                    }
                                    index <<= 1;
                                    if(hasFood) {
                                        index |= 1;
                                    }
                                    index <<= 3;
                                    switch(inFront) {
                                    case EMPTY: index |= 0; break;
                                    case WALL: index |= 1; break;
                                    case FOOD: index |= 2; break;
                                    case FRIENDLY_ANTHILL: index |= 3; break;
                                    case ENEMY_ANTHILL: index |= 4; break;
                                    case FRIEND: index |= 5; break;
                                    case ENEMY: index |= 6; break;
                                    };
                                    index <<= PHEROMONE_BITS;
                                    index |= pheromoneBits;
                                    index <<= MEMORY_BITS;
                                    index |= memoryBits;

                                    if(rules[index] != 0) {
                                        System.out.println("WTF " + index + " " + random + " " + isStacked + " " + isEast + "  " + hasFood + " " + inFront + " " + pheromoneBits + " " + memoryBits);
                                    }

                                    int result = 0;

                                    switch(output.action) {
                                    case STAY_STILL: result |= 0; break;
                                    case TURN_LEFT: result |= 1; break;
                                    case TURN_RIGHT: result |= 2; break;
                                    case MOVE_FORWARD: result |= 3; break;
                                    }
                                    result <<= PHEROMONE_BITS;
                                    for(int i = 0; i < PHEROMONE_BITS; i++) {
                                        if(output.pheromone[i]) result |= 1 << i;
                                    }
                                    result <<= MEMORY_BITS;
                                    for(int i = 0; i < MEMORY_BITS; i++) {
                                        if(output.memory[i]) result |= 1 << i;
                                    }
                                    rules[index] = result;
                                }
                            }
                        }
                    }
                }
            }
        }
        for(int rule : rules) {
            System.out.println(rule);
        }
    }
}
