
use rand::Rng;
use std::sync::Arc;
use std::io::Write;

// Direction an ant is facing
#[derive(Debug, PartialEq, Clone, Copy)]
enum Direction {
    North,
    East,
    South,
    West,
}

// All the directions, used for selecting a random one from this array
static DIRECTIONS: [Direction; 4] = [Direction::North, Direction::East, Direction::South, Direction::West];

// Methods to turn left and right
impl Direction {
    fn left(self) -> Self {
        match self {
            Direction::North => Direction::West,
            Direction::East => Direction::North,
            Direction::South => Direction::East,
            Direction::West => Direction::South,
        }
    }

    fn right(self) -> Self {
        match self {
            Direction::North => Direction::East,
            Direction::East => Direction::South,
            Direction::South => Direction::West,
            Direction::West => Direction::North,
        }
    }
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
struct Point {
    x: usize,
    y: usize,
}

#[derive(Debug, PartialEq, Clone, Copy)]
struct Bounds {
    width: usize,
    height: usize,
}

// Bounds has methods to move points in a toroidal style (i.e. pacman style, opposite edges are connected)
impl Bounds {
    fn new(width: usize, height: usize) -> Self {
        Self {
            width,
            height,
        }
    }

    fn point(self, x: usize, y: usize) -> Point {
        Point {
            x: x % self.width,
            y: y % self.height,
        }
    }

    fn move_point_in_direction(self, point: Point, direction: Direction) -> Point {
        match direction {
            Direction::North => self.point(point.x, point.y + 1),
            Direction::East => self.point(point.x + 1, point.y),
            Direction::South => self.point(point.x, point.y + self.height - 1),
            Direction::West => self.point(point.x + self.width - 1, point.y),
        }
    }

    // Toroidal distance
    fn distance(self, a: Point, b: Point) -> usize {
        (self.width + a.x - b.x) % self.width + (self.height + a.y - b.y) % self.height
    }
}


// Each non-wall tile contains N pheromone arrays where N is the number of players
#[derive(Debug, PartialEq, Clone)]
enum Tile {
    Wall,
    Empty(usize, Vec<Pheromone>),
    AntHill(usize, Vec<Pheromone>),
}

// Some pheromone access and update methods
impl Tile {
    fn pheromone(&self, owner: usize) -> Pheromone {
        match self {
            Tile::Wall => panic!("No pheromone on walls"),
            Tile::Empty(_, pheromones) => pheromones[owner],
            Tile::AntHill(_, pheromones) => pheromones[owner],
        }
    }

    fn set_pheromone(&mut self, owner: usize, new_pheromone: Pheromone) {
        match self {
            Tile::Wall => panic!("No pheromone on walls!"),
            Tile::Empty(_, pheromones) => pheromones[owner] = new_pheromone,
            Tile::AntHill(_, pheromones) => pheromones[owner] = new_pheromone,
        }
    }
}

// A PointMap is a rectangular map of vectors. It is pretty efficient because it allows us to store
// spatial location in O(1) without a hash method, and it can efficiently clear itself because it
// remembers which points have been used.
struct PointMap<T> {
    data: Vec<Vec<Vec<T>>>,
    used: Vec<Point>,
}

impl <T> PointMap<T> {
    fn new(bounds: Bounds) -> Self {
        let mut data = vec![];
        for _ in 0..bounds.width {
            let mut row = vec![];
            for _ in 0..bounds.height {
                row.push(Vec::with_capacity(16));
            }
            data.push(row);
        }
        PointMap {
            data,
            used: vec![],
        }
    }

    fn insert(&mut self, point: Point, item: T) {
        self.data[point.x][point.y].push(item);
        self.used.push(point);
    }

    fn get(&self, point: Point) -> &Vec<T> {
        &self.data[point.x][point.y]
    }

    fn get_mut(&mut self, point: Point) -> &mut Vec<T> {
        &mut self.data[point.x][point.y]
    }

    fn clear(&mut self) {
        for point in self.used.drain(..) {
            self.data[point.x][point.y].clear();
        }
    }
}

// Struct representing the game board
struct Map {
    bounds: Bounds,
    tiles: Vec<Vec<Tile>>,
}

impl Map {

    fn new(bounds: Bounds, players: usize) -> Map {
        Map {
            bounds,
            tiles: vec![vec![Tile::Empty(0, vec![NEW_PHEROMONE; players]); bounds.height]; bounds.width],
        }
    }

    // We create the map iteratively we so need some mutative methods
    fn place_wall(&mut self, point: Point) {
        self.tiles[point.x][point.y] = Tile::Wall;
    }

    fn place_anthill(&mut self, owner: usize, point: Point) {
        let tile = &mut self.tiles[point.x][point.y];
        let new_tile = match tile {
            Tile::Empty(_, pheromones) => Tile::AntHill(owner, pheromones.clone()),
            _ => panic!("Cannot place anthill on non-empty tile")
        };
        *tile = new_tile;
    }

    // Used so we don't accidentally wall off an anthill -- all hills need to be reachable from each other
    fn is_reachable(&self, visited: &mut PointMap<()>, start: Point, end: Point) -> bool {
        for max_depth in 0..self.bounds.width*self.bounds.height {
            if self.ida(visited, 0, max_depth, start, end) {
                return true;
            }
            visited.clear();
        }
        false
    }

    fn ida(&self, visited: &mut PointMap<()>, depth: usize, max_depth: usize, node: Point, goal: Point) -> bool {
        if node == goal {
            return true;
        }
        if depth + self.bounds.distance(node, goal) > max_depth {
            return false;
        }
        if self.tiles[node.x][node.y] == Tile::Wall {
            return false;
        }
        if visited.get(node).len() > 0 {
            return false;
        }
        visited.insert(node, ());
        self.ida(visited, depth+1, max_depth, self.bounds.move_point_in_direction(node, Direction::North), goal) ||
            self.ida(visited, depth+1, max_depth, self.bounds.move_point_in_direction(node, Direction::East), goal) ||
            self.ida(visited, depth+1, max_depth, self.bounds.move_point_in_direction(node, Direction::South), goal) ||
            self.ida(visited, depth+1, max_depth, self.bounds.move_point_in_direction(node, Direction::West), goal)
    }

    fn spawn_food(&mut self, mut food_to_add: impl FnMut() -> usize) {
        for x in 0..self.bounds.width {
            for y in 0..self.bounds.height {
                if let Tile::Empty(food_count, _) = &mut self.tiles[x][y] {
                    *food_count += food_to_add();
                }
            }
        }
    }

    // True if we successfully took a food, false otherwise
    fn try_to_take_food_from(&mut self, point: Point) -> bool {
        if let Tile::Empty(food_count, _) = &mut self.tiles[point.x][point.y] {
            if *food_count > 0 {
                *food_count -= 1;
                return true;
            }
        }
        false
    }

    // Gets the "InFront" input for an ant given the point that is in front of it
    fn in_front(&self, owner: usize, point: Point) -> InFront {
        match self.tiles[point.x][point.y] {
            Tile::Empty(0, _) => InFront::Empty,
            Tile::Empty(_, _) => InFront::Food,
            Tile::Wall => InFront::Wall,
            Tile::AntHill(hill_owner, _) => if hill_owner == owner {
                InFront::FriendlyAntHill
            } else {
                InFront::EnemyAntHill
            }
        }
    }

    fn pheromone(&self, owner: usize, point: Point) -> Pheromone {
        self.tiles[point.x][point.y].pheromone(owner)
    }

    fn set_pheromone(&mut self, owner: usize, point: Point, pheromone: Pheromone) {
        self.tiles[point.x][point.y].set_pheromone(owner, pheromone);
    }

    fn is_wall(&self, point: Point) -> bool {
        self.tiles[point.x][point.y] == Tile::Wall
    }

    fn is_owned_ant_hill(&self, owner: usize, point: Point) -> bool {
        if let Tile::AntHill(hill_owner, _) = self.tiles[point.x][point.y] {
            hill_owner == owner
        } else {
            false
        }
    }
}

// Convert a slice of bits to a number (that is those bits in binary)
pub fn slice_to_usize(slice: &[bool]) -> usize {
    let mut result = 0;
    for i in 0..slice.len() {
        if slice[i] {
            result |= 1 << i;
        }
    }
    result
}

const MEMORY_BITS: usize = 8;
type Memory = [bool; MEMORY_BITS];
const NEW_MEMORY: Memory = [false; MEMORY_BITS];

fn set_memory(memory: &mut Memory, memory_bits: usize) {
    for i in 0..MEMORY_BITS {
        memory[i] = memory_bits & (1 << i) != 0;
    }
}

const PHEROMONE_BITS: usize = 6;
type Pheromone = [bool; PHEROMONE_BITS];
const NEW_PHEROMONE: Pheromone = [false; PHEROMONE_BITS];

fn set_pheromone(pheromone: &mut Pheromone, pheromone_bits: usize)  {
    for i in 0..PHEROMONE_BITS {
        pheromone[i] = pheromone_bits & (1 << i) != 0;
    }
}

// Ant and its state -- for laziness in the code below we also store the ant's intended location
// inside the ant rather than maintain a mapping.
#[derive(Debug, PartialEq, Clone, Copy)]
struct Ant {
    owner: usize,
    direction: Direction,
    location: Point,
    next_location: Point,
    has_food: bool,
    memory: Memory,
}

impl Ant {
    fn is_moving(&self) -> bool {
        self.location != self.next_location
    }
}

#[derive(Debug, PartialEq, Clone, Copy)]
pub enum InFront {
    Empty,
    Wall,
    Food,
    FriendlyAntHill,
    EnemyAntHill,
    Friend,
    Enemy,
}

#[derive(Debug, PartialEq, Clone, Copy)]
pub struct Input {
    pub memory: Memory,
    pub pheromone: Pheromone,
    pub in_front: InFront,
    pub has_food: bool,
    pub is_east: bool,
    pub is_stacked: bool,
    pub random: bool,
}

impl Input {
    fn serialize(input: &Input) -> usize {
        let mut result = 0;
        if input.random {
            result |= 1;
        }
        result <<= 1;
        if input.is_stacked {
            result |= 1;
        }
        result <<= 1;
        if input.is_east {
            result |= 1;
        }
        result <<= 1;
        if input.has_food {
            result |= 1;
        }
        result <<= 3;
        result |= match input.in_front {
            InFront::Empty => 0,
            InFront::Wall => 1,
            InFront::Food => 2,
            InFront::FriendlyAntHill => 3,
            InFront::EnemyAntHill => 4,
            InFront::Friend => 5,
            InFront::Enemy => 6,
        };
        result <<= PHEROMONE_BITS;
        result |= slice_to_usize(&input.pheromone);
        result <<= MEMORY_BITS;
        result |= slice_to_usize(&input.memory);
        result
    }
}

#[derive(Debug, PartialEq, Clone, Copy)]
pub enum Action {
    StayStill,
    TurnLeft,
    TurnRight,
    MoveForward,
}

#[derive(Debug, PartialEq, Clone, Copy)]
pub struct Output {
    pub memory: Memory,
    pub pheromone: Pheromone,
    pub action: Action,
}

impl Output {
    fn deserialize(mut output: u16) -> Output {
        let mut memory = NEW_MEMORY;
        set_memory(&mut memory, output as usize % (1 << MEMORY_BITS));
        output >>= MEMORY_BITS;
        let mut pheromone = NEW_PHEROMONE;
        set_pheromone(&mut pheromone, output as usize % (1 << PHEROMONE_BITS));
        output >>= PHEROMONE_BITS;
        let action = match output {
            0 => Action::StayStill,
            1 => Action::TurnLeft,
            2 => Action::TurnRight,
            3 => Action::MoveForward,
            _ => unreachable!(),
        };
        Output {
            memory,
            pheromone,
            action,
        }
    }
}

#[derive(Clone)]
pub struct AI {
    rules: Arc<Vec<u16>>,
}

impl AI {

    pub fn from_raw(rules: Vec<u16>) -> AI {
        assert_eq!(1 << 21, rules.len());
        AI {
            rules: Arc::new(rules),
        }
    }

    fn decide(&self, input: &Input) -> Output {
        let index = Input::serialize(&input);
        let output = self.rules[index];
        Output::deserialize(output)
    }
}

pub struct Game<R> {
    random: R,
    map: Map,
    ants: Vec<Ant>,
    ais: Vec<AI>,

    owners_by_location: PointMap<usize>,
    ants_by_next_location: PointMap<usize>,
    ants_by_location: PointMap<usize>,
}

impl <R: Rng> Game<R> {

    pub fn generate(mut random: R, width: usize, height: usize, ais: Vec<AI>) -> Game<R> {

        // If we accidentally make an illegal map, we'll just keep looping 'till we don't...
        'gen: loop {
            let bounds = Bounds::new(width, height);

            let mut all_points = vec![];
            for x in 0..width {
                for y in 0..height {
                    all_points.push(bounds.point(x, y));
                }
            }

            random.shuffle(&mut all_points);

            let mut map = Map::new(bounds, ais.len());
            let mut ants = vec![];

            let mut ant_hill_locations = vec![];

            for owner in 0..ais.len() {
                let point = all_points.pop().unwrap();
                map.place_anthill(owner, point);
                ant_hill_locations.push(point);
                ants.push(Ant {
                    owner,
                    direction: *random.choose(&DIRECTIONS).unwrap(),
                    location: point,
                    next_location: point,
                    has_food: false,
                    memory: NEW_MEMORY,
                });
            }

            // 1/4 of the tiles walls
            for _ in 0..all_points.len() / 4 {
                let point = all_points.pop().unwrap();
                map.place_wall(point);
            }

            // If all anthills not reachable from each other, try another map
            let mut visited = PointMap::new(bounds);
            for &end in &ant_hill_locations[1..] {
                visited.clear();
                if !map.is_reachable(&mut visited, ant_hill_locations[0], end) {
                    continue 'gen;
                }
            }

            // Success
            break Game {
                random,
                map,
                ants,
                ais,

                owners_by_location: PointMap::new(bounds),
                ants_by_next_location: PointMap::new(bounds),
                ants_by_location: PointMap::new(bounds),
            }
        }
    }

    pub fn tick(&mut self) {
        let random = &mut self.random;

        self.map.spawn_food(|| if random.next_f64() < 0.0001 { 5 } else { 0 });

        random.shuffle(&mut self.ants);

        self.owners_by_location.clear();
        self.ants_by_next_location.clear();
        self.ants_by_location.clear();

        for ant in &self.ants {
            let owners = self.owners_by_location.get_mut(ant.location);
            if owners.is_empty() {
                *owners = vec![0; self.ais.len()];
            }
            owners[ant.owner] += 1;
        }

        // Collect all of the ant's intended actions
        for ant in &mut self.ants {
            if !ant.has_food {
                ant.has_food = self.map.try_to_take_food_from(ant.location);
            }

            let forward = self.map.bounds.move_point_in_direction(ant.location, ant.direction);

            let in_front = if self.owners_by_location.get(forward).is_empty() {
                self.map.in_front(ant.owner, forward)
            } else if self.owners_by_location.get(forward)[ant.owner] > 0 {
                InFront::Friend
            } else {
                InFront::Enemy
            };

            let input = Input {
                memory: ant.memory,
                pheromone: self.map.pheromone(ant.owner, ant.location),
                in_front,
                has_food: ant.has_food,
                is_east: ant.direction == Direction::East,
                is_stacked: self.owners_by_location.get(ant.location)[ant.owner] > 1,
                random: random.gen(),
            };

            let output = self.ais[ant.owner].decide(&input);
            ant.memory = output.memory;
            self.map.set_pheromone(ant.owner, ant.location, output.pheromone);
            ant.next_location = ant.location;
            match output.action {
                Action::MoveForward => if !self.map.is_wall(forward) {
                    ant.next_location = forward;
                },
                Action::TurnLeft => {
                    ant.direction = ant.direction.left();
                },
                Action::TurnRight => {
                    ant.direction = ant.direction.right();
                }
                Action::StayStill => (),
            }
        }

        let mut is_alive = vec![true; self.ants.len()];
        for (i, ant) in self.ants.iter().enumerate() {
            self.ants_by_next_location.insert(ant.next_location, i);
            self.ants_by_location.insert(ant.location, i);
        }

        // Mark ants moving into each other dead
        for i in 0..self.ants.len() {
            if is_alive[i] {
                let ant = self.ants[i];
                for &j in self.ants_by_location.get(ant.next_location) {
                    if is_alive[j] {
                        let other = self.ants[j];
                        if other.next_location == ant.location && other.owner != ant.owner {
                            assert!(ant.is_moving());
                            assert!(other.is_moving());
                            is_alive[i] = false;
                            is_alive[j] = false;
                            break;
                        }
                    }
                }
            }
        }

        // Mark ants landing on the same square dead
        for i in 0..self.ants.len() {
            if is_alive[i] {
                let ant = self.ants[i];
                if ant.is_moving() {
                    for &j in self.ants_by_next_location.get(ant.next_location) {
                        if is_alive[j] {
                            let other = self.ants[j];
                            if other.is_moving() && other.owner != ant.owner {
                                is_alive[i] = false;
                                is_alive[j] = false;
                                break;
                            }
                        }
                    }
                }
            }
        }

        // Handle the case of non-moving ants requiring 2 ants to kill
        for i in 0..self.ants.len() {
            if is_alive[i] {
                let ant = self.ants[i];
                if !ant.is_moving() {
                    let mut hit_already = false;
                    for &j in self.ants_by_next_location.get(ant.location) {
                        if is_alive[j] {
                            let other = self.ants[j];
                            if other.owner != ant.owner {
                                assert!(other.is_moving());
                                is_alive[j] = false;
                                if hit_already {
                                    is_alive[i] = false;
                                    break;
                                }
                                hit_already = true;
                            }
                        }
                    }
                }
            }
        }

        // Loop through to create new ants, drop off food, spawn new ants, etc.
        let mut new_ants = vec![];

        for i in 0..self.ants.len() {
            if is_alive[i] {
                let mut ant = self.ants[i];
                ant.location = ant.next_location;
                if ant.has_food {
                    if self.map.is_owned_ant_hill(ant.owner, ant.location) {
                        new_ants.push(Ant {
                            owner: ant.owner,
                            direction: *random.choose(&DIRECTIONS).unwrap(),
                            location: ant.location,
                            next_location: ant.location,
                            has_food: false,
                            memory: NEW_MEMORY,
                        });
                        ant.has_food = false;
                    }
                }
                new_ants.push(ant);
            }
        }

        self.ants = new_ants;
    }

    pub fn result(&self) -> GameResult {
        let mut ant_counts = vec![0; self.ais.len()];

        if self.ants.is_empty() {
            return GameResult::Tie;
        }

        for ant in &self.ants {
            ant_counts[ant.owner] += 1;
        }

        for (i, count) in ant_counts.iter().enumerate() {
            if *count == self.ants.len() {
                return GameResult::Winner(i);
            }
        }

        GameResult::None
    }


    // Conveniently, each tile can be encoded in 16 bits, so we just encode each tile as a number.
    // We could probably do better by base64 encoding it, but this is ok for now.
    pub fn write_json(&mut self, output: &mut impl Write) {
        let mut tiles = vec![];

        for x in 0..self.map.bounds.width {
            let mut column = vec![];
            for y in 0..self.map.bounds.height {
                let representation = match self.map.tiles[x][y] {
                    Tile::Wall => TileRepresentation::Wall,
                    Tile::Empty(food, _) => TileRepresentation::Empty { food, ants: TileAnts::default() },
                    Tile::AntHill(owner, _) => TileRepresentation::AntHill { owner, ants: TileAnts::default() },
                };
                column.push(representation);
            }
            tiles.push(column);
        }

        for ant in &self.ants {
            let ants: &mut TileAnts = match &mut tiles[ant.location.x][ant.location.y] {
                TileRepresentation::Wall => unreachable!(),
                TileRepresentation::Empty { ref mut ants, .. } => ants,
                TileRepresentation::AntHill { ref mut ants, .. } => ants,
            };
            match ant.direction {
                Direction::North => { ants.north_ant = true; ants.north_food |= ant.has_food; },
                Direction::East => { ants.east_ant = true; ants.east_food |= ant.has_food; },
                Direction::South => { ants.south_ant = true; ants.south_food |= ant.has_food; },
                Direction::West => { ants.west_ant = true; ants.west_food |= ant.has_food; },
            }
            ants.ants_owner = ant.owner;
        }

        write!(output, "[").unwrap();
        for x in 0..self.map.bounds.width {
            if x > 0 {
                write!(output, ",").unwrap();
            }
            write!(output, "[").unwrap();
            for y in 0..self.map.bounds.height {
                if y > 0 {
                    write!(output, ",").unwrap();
                }
                write!(output, "{}", u16::from(tiles[x][y])).unwrap();
            }
            write!(output, "]").unwrap();
        }
        write!(output, "]").unwrap();
    }
}

#[derive(Clone, Copy, Default)]
pub struct TileAnts {
    ants_owner: usize,
    north_ant: bool,
    east_ant: bool,
    south_ant: bool,
    west_ant: bool,
    north_food: bool,
    east_food: bool,
    south_food: bool,
    west_food: bool,
}

#[derive(Clone, Copy)]
pub enum TileRepresentation {
    Wall,
    AntHill {
        owner: usize,
        ants: TileAnts,
    },
    Empty {
        food: usize,
        ants: TileAnts,
    },
}

impl From<TileRepresentation> for u16 {
    fn from(representation: TileRepresentation) -> Self {
        let (tag, number, ants) = match representation {
            TileRepresentation::Wall => return 0,
            TileRepresentation::AntHill { owner, ants } => (1, owner, ants),
            TileRepresentation::Empty { food, ants } => (2, 7.min(food), ants),
        };
        let mut result = tag;
        let mut i = 2;
        assert!(number <= 7);
        result |= (number as u16) << i;
        i += 3;
        assert!(ants.ants_owner <= 7);
        result |= (ants.ants_owner as u16) << i;
        i += 3;

        for &bit in &[ants.north_ant, ants.east_ant, ants.south_ant, ants.west_ant, ants.north_food, ants.east_food, ants.south_food, ants.west_food] {
            if bit {
                result |= 1 << i;
            }
            i+= 1;
        }

        result
    }
}

#[derive(Debug, PartialEq, Clone, Copy)]
pub enum GameResult {
    None,
    Tie,
    Winner(usize),
}