extern crate rand;

mod game;

use game::{AI, Game, GameResult};

use rand::{ Rng, SeedableRng, XorShiftRng, thread_rng };

use std::io::{BufRead, BufReader, BufWriter};
use std::fs::File;
use std::path::{PathBuf, Path};
use std::io::Write;

fn tournament(mut random: impl Rng, output_dir: Option<String>, ais: &[AI], ai_names: &[String], game_count: usize, board_size: usize) -> Vec<f64> {

    let ai_count = ais.len();

    let mut args = vec![];

    let mut indexes = (0..ai_count).collect::<Vec<_>>();

    while args.len() < game_count {
        let players_in_game = 2 + (random.next_f64() * (ai_count-2) as f64) as usize;
        random.shuffle(&mut indexes);

        let mut players = vec![];
        for &i in &indexes[0..players_in_game] {
            players.push((i, ais[i].clone(), ai_names[i].clone()));
        }
        args.push((XorShiftRng::from_seed(random.gen()), players));
    }

    let thread_count = 8;

    let games_per_thread = (game_count + thread_count - 1) / thread_count;

    let mut handles = vec![];

    for _ in 0..thread_count {
        let mut thread_args = vec![];
        for _ in 0..games_per_thread {
            if let Some(arg) = args.pop() {
                thread_args.push(arg);
            } else {
                break;
            }
        }

        let output_dir = output_dir.clone();

        let handle = ::std::thread::spawn(move || {
            let mut thread_rng = thread_rng();
            let mut winners = vec![0; ai_count];
            for (r, players) in thread_args {
                let mut indexes = vec![];
                let mut ais = vec![];

                for (i, ai, _) in &players {
                    indexes.push(i.clone());
                    ais.push(ai.clone());
                }

                let mut filename = format!("{}", thread_rng.next_u32());

                let mut game = Game::generate(r, board_size, board_size, ais);

                let mut output = output_dir.as_ref().map(|dir| {
                    let mut full_path = PathBuf::new();
                    full_path.push(&dir);
                    full_path.push(&filename);
                    let file = File::create(full_path).expect("Unable to create file");
                    BufWriter::new(file)
                });


                if let Some(output) = &mut output {
                    write!(output, "{{\nplayer_names: [\"{}\"", players[0].2).unwrap();
                    for (_, _, player_name) in &players[1..] {
                        write!(output, ", \"{}\"", player_name).unwrap();
                    }
                    write!(output, "],\nframes: [\n").unwrap();
                }

                for t in 0..10000 {
                    if let Some(output) = &mut output {
                        if t != 0 {
                            write!(output, "\n,").unwrap();
                        }
                        game.write_json(output);
                    }
                    if game.result() != GameResult::None {
                        break;
                    }
                    game.tick();
                }

                if let Some(output) = &mut output {
                    write!(output, "]}}\n").unwrap();
                    output.flush().unwrap();

                    let mut new_filename = format!("{}-[", thread_rng.next_u32());
                    new_filename += &match game.result() {
                        GameResult::Winner(i) => players[i].2.to_string(),
                        _ => "Tie".to_string(),
                    };
                    new_filename += "]";
                    for player_name in players.iter().map(|x| &x.2) {
                        new_filename += "-";
                        new_filename += player_name;
                    }

                    let dir = output_dir.clone().unwrap();
                    let original_file: PathBuf = [&dir, &filename].iter().collect();
                    let new_file: PathBuf = [&dir, &new_filename].iter().collect();
                    ::std::fs::rename(original_file, new_file).unwrap();
                }

                if let GameResult::Winner(i) = game.result() {
                    winners[indexes[i]] += 1;
                }

            }
            winners
        });
        handles.push(handle);
    }
    let mut results = vec![0.0; ais.len()];
    for handle in handles {
        let thread_results = handle.join().unwrap();
        for (i, &wins) in thread_results.iter().enumerate() {
            results[i] += wins as f64;
        }
    }

    let total = results.iter().sum::<f64>();

    for i in 0..ais.len() {
        results[i] *= 100.0 / total
    }
    results
}

fn main() {

    let help = "usage: valiant games_n output_dir ai_file ai_file ... ai_file";

    let mut args = ::std::env::args();

    args.next();

    let game_count = args.next().expect(help).parse::<usize>().expect("Unable to parse game count");
    let output_dir = args.next().expect(help);
    let output_dir = if output_dir == "-" {
        None
    } else {
        Some(output_dir)
    };

    let mut ais = vec![];
    let mut ai_names = vec![];

    for ai_file in args {
        let path: &Path = ai_file.as_ref();
        let mut file = ::std::fs::File::open(path).expect(&format!("Unable to open {}", ai_file));
        let mut reader = BufReader::new(file);
        let mut ai = vec![0u16; 1 << 21];
        for (i, line) in reader.lines().map(|line| line.unwrap()).enumerate() {
            let rule = line.parse::<u16>().expect(&format!("Unable to parse rule {}", line));
            ai[i] = rule;
        }
        ais.push(AI::from_raw(ai));
        ai_names.push(path.file_name().unwrap().to_str().unwrap().to_owned());
    }

    assert!(ais.len() >= 2, "Expected at least 2 AIs");

    let random = thread_rng();

    let results = tournament(random, output_dir, &ais, &ai_names, game_count, 24);

    for i in 0..ais.len() {
        println!("{}: {}", ai_names[i], results[i]);
    }
}
