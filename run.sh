#!/bin/bash
# edit this script to your liking for your ant name
# also comment out the last line if you don't want it to commit/push for you

if [ ! -d valiant_players ]
then
    git clone https://gitlab.com/grantslatton/valiant_players.git
fi
javac generate/java/Generate.java &&
    cd generate/java/ &&
    java Generate > MYANTNAME.ant &&
    mv ./MYANTNAME.ant ../../valiant_players/players/
    cd - &&
    cd valiant_players &&
    git add players/MYANTNAME.ant &&
    git commit -m "updated ant" &&
    git pull && git commit || git push && cd ..


